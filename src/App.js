import * as React from "react";
import Layout from "./Component/Layout/Layout";
import BurgerBuilder from "./Container/BurgerBuilder/BurgerBuilder";
import Orders from "./Container/Orders/Orders";
import { Route, BrowserRouter as Router, Switch } from "react-router-dom";
import Login from "./Container/Login/Login";
import SignUp from "./Container/SignUp/SignUp";
import AuthProvider from "./Context/AuthContext";
import { createStore } from "redux";
import { Reducer } from "./Store/reducer";
import { Provider } from "react-redux";
import Spinner from "./Component/UI/Spinner/Spinner";

const reduxStore = createStore(Reducer);

function App() {
  return (
    <Provider store={reduxStore}>
      {/*<AuthProvider>*/}
      <Router>
        <Spinner />
        <Layout>
          <Switch>
            <Route path={"/Orders"} component={Orders} />
            <Route path={"/BurgerBuilder"} component={BurgerBuilder} />
            <Route exact path={"/"} component={BurgerBuilder} />
            <Route path={"/Login"} component={Login} />
            <Route path={"/SignUp"} component={SignUp} />
          </Switch>
        </Layout>
      </Router>
      {/*</AuthProvider>*/}
    </Provider>
  );
}

export default App;
