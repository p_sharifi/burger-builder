import React from "react";

export const AuthContext = React.createContext({
  setLogin: () => {}
});

function AuthProvider(props) {
  const [isLogin, setLogin] = React.useState(false);

  return (
    <AuthContext.Provider value={{ isLogin, setLogin }}>
      {props.children}
    </AuthContext.Provider>
  );
}

export default AuthProvider;
