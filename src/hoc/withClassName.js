import * as React from "react";

function withClassName(WrappedComponent, className) {
  return props => (
    <div className={className}>
      <WrappedComponent {...props} />
    </div>
  );
}

export default withClassName;
