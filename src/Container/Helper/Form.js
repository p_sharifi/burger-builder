export function validateForm(...inputs) {
  let isValid = true;
  inputs.forEach(item => (isValid = item.isValid(item.value) && isValid));
  return isValid;
}
