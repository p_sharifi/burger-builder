import * as React from "react";
import classes from "./Price.module.css";

class Price extends React.Component {
  render() {
    return (
      <div className={classes.Price}>Total Price = {this.props.value}</div>
    );
  }
}

export default Price;
