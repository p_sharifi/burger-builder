import * as React from "react";
import "./BurgerView.css";

function BurgerView(props) {
  const { meat, cheese, salad } = props;
  const meats = [];
  for (let i = 0; i < meat; i++) {
    meats.push(<div key={i} className="Meat"></div>);
  }
  const cheeses = [];
  for (let i = 0; i < cheese; i++) {
    cheeses.push(<div key={i} className="Cheese"></div>);
  }

  const salads = [];
  for (let i = 0; i < salad; i++) {
    salads.push(<div key={i} className="Salad"></div>);
  }

  const isEmpty = cheeses.length + salads.length + meats.length === 0;
  return (
    <div className="Container">
      <div className="BreadTop">
        <div className="Seeds1"></div>
        <div className="Seeds2"></div>
      </div>
      {isEmpty && (
        <div
          style={{ textAlign: "center", fontWeight: "bold", color: "deeppink" }}
        >
          Please Make Your Delicious Burger
        </div>
      )}
      {meats}
      {salads}
      {cheeses}
      <div className="BreadBottom"></div>
    </div>
  );
}

export default BurgerView;
