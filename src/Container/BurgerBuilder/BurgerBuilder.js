import * as React from "react";
import classes from "./BurgerBuilder.module.css";
import BurgerView from "./BurgerView/BurgerView";
import Price from "./Price/Price";
import Controls from "./Controls/Controls";
import Button from "../../Component/UI/Button/TheButton";
import axios from "../../axios";
import { connect } from "react-redux";
import { setLoading } from "../../Store/actionCreator";
class BurgerBuilder extends React.Component {
  constructor(props) {
    super(props);
    this.state = this.initState;
    console.log(props);
  }
  initState = {
    meat: 0,
    salad: 0,
    cheese: 0,
    order_number: 0
  };
  addHandler = type => {
    if (type === "meat" && this.state.meat < 3) {
      this.setState({
        meat: this.state.meat + 1
      });
    }
    if (type === "salad" && this.state.salad < 3) {
      this.setState({
        salad: this.state.salad + 1
      });
    }
    if (type === "cheese" && this.state.cheese < 3) {
      this.setState({
        cheese: this.state.cheese + 1
      });
    }
  };

  removeHandler = type => {
    if (type === "meat" && this.state.meat > 0) {
      this.setState({
        meat: this.state.meat - 1
      });
    }
    if (type === "salad" && this.state.salad > 0) {
      this.setState({
        salad: this.state.salad - 1
      });
    }
    if (type === "cheese" && this.state.cheese > 0) {
      this.setState({
        cheese: this.state.cheese - 1
      });
    }
  };

  calculatePrice = () => {
    let initPrice = 5000;
    let meatPrice = 5000;
    let saladPrice = 2000;
    let cheesePrice = 4000;

    return (
      initPrice +
      this.state.meat * meatPrice +
      this.state.salad * saladPrice +
      this.state.cheese * cheesePrice
    );
  };

  okHandler = () => {
    const { meat, cheese, salad } = this.state;
    const orderModel = {
      salad,
      cheese,
      meat,
      total_price: this.calculatePrice()
    };
    this.props.setLoading(true);
    axios
      .post("/Order/AddOrder", orderModel)
      .then(res => {
        if (res.data.status) {
          this.setState({
            order_number: res.data.order_number
          });
          this.props.setLoading(false);
        } else {
          this.setState({
            order_number: -1
          });
          this.props.setLoading(false);
        }
      })
      .catch(err => {
        this.setState({
          order_number: -1
        });
        this.props.setLoading(false);
      });
  };

  resetHandler = () => {
    this.setState(this.initState);
  };

  render() {
    const { meat, cheese, salad } = this.state;
    return (
      <div className={classes.Container}>
        <BurgerView meat={meat} salad={salad} cheese={cheese} />
        <Price Title={Price} value={this.calculatePrice()} />
        <Controls
          meat={meat}
          cheese={cheese}
          salad={salad}
          addHandler={this.addHandler}
          removeHandler={this.removeHandler}
        />
        <div style={{ textAlign: "center" }}>
          <Button label="Order" onClick={this.okHandler} />
          <Button label="Reset" onClick={this.resetHandler} />
        </div>
        {this.state.order_number > 0 && (
          <div className={classes.SuccessMessage}>
            Your Order successfully Registered.Your OrderNumber is :{" "}
            {this.state.order_number}{" "}
          </div>
        )}
        {this.state.order_number < 0 && (
          <div className={classes.ErrorMessage}>
            An Error has Occurred. Please call Admin to Help.{" "}
          </div>
        )}
        }
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    loading: state.loading
  };
};
const mapDispatchToProps = dispatch => {
  return {
    setLoading: isLoading => dispatch(setLoading(isLoading))
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BurgerBuilder);

// const map = connect(mapStateToProps, mapDispatchToProps);
// export default map(BurgerBuilder)
