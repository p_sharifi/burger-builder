import * as React from "react";
import classes from "./InputControls.module.css";

class InputControls extends React.Component {
  add = () => {
    this.props.addHandler(this.props.type);
  };

  remove = () => {
    this.props.removeHandler(this.props.type);
  };
  render() {
    const { type, count } = this.props;

    return (
      <div className={classes.Container}>
        <span className={classes.Title}>{type}</span>
        <button onClick={this.add}>+</button>
        <span>{count}</span>
        <button onClick={this.remove}>-</button>
      </div>
    );
  }
}

export default InputControls;
