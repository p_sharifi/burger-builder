import * as React from "react";
import InputControls from "./InputControls/InputControls";
import clasess from "./Controls.module.css";

class Controls extends React.Component {
  render() {
    return (
      <div className={clasess.Controls}>
        <InputControls
          type="meat"
          count={this.props.meat}
          addHandler={this.props.addHandler}
          removeHandler={this.props.removeHandler}
        />
        <InputControls
          type="cheese"
          count={this.props.cheese}
          addHandler={this.props.addHandler}
          removeHandler={this.props.removeHandler}
        />
        <InputControls
          type="salad"
          count={this.props.salad}
          addHandler={this.props.addHandler}
          removeHandler={this.props.removeHandler}
        />
      </div>
    );
  }
}

export default Controls;
