import * as React from "react";
import classes from "./Login.module.css";
import axios from "../../axios";
import Input from "../../Component/UI/Input/Input";
import { useFormInput } from "../hooks/useFormInput";
import { validateForm } from "../Helper/Form";
import { AuthContext } from "../../Context/AuthContext";
import useRedux from "../hooks/useRedux";

const Login = props => {
  const username = useFormInput("alirezaed", true);
  const password = useFormInput("123456", true);

  const { setLoading, setLogin } = useRedux();
  const loginClickHandler = () => {
    if (!validateForm(username, password)) {
      return;
    }
    setLoading(true);
    axios
      .post("User/login", {
        username: username.value,
        password: password.value
      })
      .then(result => {
        if (result.data.status) {
          setLogin(true, result.data.message);
          props.history.push("/BurgerBuilder");
        } else {
          localStorage.removeItem("token");
          setLogin(false);
        }
        setLoading(false);
      })
      .catch(err => {
        alert("Call Support Team");
        setLoading(false);
      });
  };

  return (
    <div className={classes.Container}>
      <h2>Login Form</h2>
      <form className={classes.Form}>
        <div>
          <Input
            {...username}
            type="text"
            placeholder="Please Enter your UserName"
            label={"Username"}
            required={true}
          />

          <Input
            {...password}
            label={"Password  "}
            type="password"
            placeholder="Enter Password"
            required={true}
          />

          <button
            onClick={loginClickHandler}
            className={classes.Button}
            type="button"
          >
            Login
          </button>
        </div>
      </form>
    </div>
  );
};

export default Login;
