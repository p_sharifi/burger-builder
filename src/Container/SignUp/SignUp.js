import * as React from "react";
import classes from "./SignUp.module.css";
import Input from "../../Component/UI/Input/Input";
import { useFormInput } from "../hooks/useFormInput";
import { validateForm } from "../Helper/Form";
import axios from "../../axios";

const SignUp = props => {
  const username = useFormInput("", true);
  const password = useFormInput("", true);
  const confirmPassword = useFormInput("", true);
  const email = useFormInput("", true);
  const fullName = useFormInput("", true);

  const signUpClickHandler = () => {
    if (!validateForm(username, password, confirmPassword, email, fullName))
      return;
    axios
      .post("/User/SignUp", {
        username: username.value,
        password: password.value,
        fullName: fullName.value,
        email: email.value
      })
      .then(res => console.log(res));
  };

  return (
    <div className={classes.Container}>
      <h2>SignUp Form</h2>
      <form className={classes.Form}>
        <div>
          <Input
            {...username}
            label={"UserName"}
            type="text"
            placeholder="Enter UserName"
          />
          <Input
            {...password}
            label={"Password"}
            type="password"
            placeholder="Enter Password"
          />
          <Input
            {...confirmPassword}
            label={"ConfirmPassword"}
            type="password"
            placeholder="Enter Password"
          />
          <Input
            {...email}
            label={"Email"}
            type="text"
            placeholder="Enter your Email"
          />
          <Input
            {...fullName}
            label={"FullName"}
            type="text"
            placeholder="Enter your Name"
          />
          <button
            onClick={signUpClickHandler}
            className={classes.Button}
            type="button"
          >
            SignUp
          </button>
        </div>
      </form>
    </div>
  );
};

export default SignUp;
