import * as actions from "../../Store/actionCreator";
import { useDispatch } from "react-redux";

function useRedux() {
  const dispatch = useDispatch();
  function setLoading(loading) {
    dispatch(actions.setLoading(loading));
  }
  function setLogin(isLogin, token) {
    dispatch(actions.setLogin(isLogin));
    if (isLogin && token) localStorage.setItem("token", token);
    else localStorage.removeItem("token");
  }
  return {
    setLoading,
    setLogin
  };
}

export default useRedux;
