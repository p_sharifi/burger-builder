import * as React from "react";

export function useFormInput(initialValue, required) {
  const [value, setValue] = React.useState(initialValue);
  const [validateMessage, setValidateMessage] = React.useState("");

  const handleValueChanged = e => {
    setValue(e.target.value);
    isValid(e.target.value);
  };

  const isValid = input => {
    let message = "";
    if (required && input === "") {
      message = "this field is required";
    }
    setValidateMessage(message);
    return message === "";
  };

  return {
    value,
    validateMessage,
    onChange: handleValueChanged,
    isValid
  };
}
