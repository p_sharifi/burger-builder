import * as React from "react";
import classes from "./Orders.module.css";
import Table from "./Table/Table";
import { useReducer, useState } from "react";
import axios from "../../axios";

function Orders() {
  const reducer = (state, action) => {
    switch (action.type) {
      case "SET_ORDERS":
        return {
          orders: action.payload.orders,
          totalRecordsCount: action.payload.totalRecordsCount,
          loading: false
        };

      case "SET-LOADING":
        return {
          ...state,
          loading: true
        };
    }
  };

  const [state, dispatch] = useReducer(reducer, {
    orders: [],
    totalRecordsCount: 0,
    loading: true
  });
  const { orders, totalRecordsCount, loading } = state;

  React.useEffect(() => {
    console.log("effect");
    axios.post("/Order/GetAllOrders", {}).then(result => {
      if (result.status === true)
        dispatch({
          type: "SET_ORDERS",
          payload: {
            orders: result.data.list,
            totalRecordsCount: result.data.total_count
          }
        });
    });
  });

  const headers = React.useRef([
    { title: "Order Number", field_name: "order_number", sortable: true },
    { title: "Creation Date", field_name: "create_date", sortable: false },
    { title: "Total Price", field_name: "total_price", sortable: true }
  ]);

  const refreshHandler = input => {
    console.log("refresh");
    axios.post("/SafeOrder/GetAllOrders", input).then(result => {
      dispatch({
        type: "SET_ORDERS",
        payload: {
          orders: result.data.list,
          totalRecordsCount: result.data.total_count
        }
      });
    });
  };

  return (
    <div>
      <Table
        headers={headers.current}
        data={orders}
        onRefresh={refreshHandler}
        totalRecordsCount={totalRecordsCount}
      />
    </div>
  );
}

export default Orders;
