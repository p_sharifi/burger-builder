import * as React from "react";
import classes from "./Table.module.css";
import Pagination from "./Pagination/Pagination";

class Table extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      sort_field: "order_number",
      sort_order: "desc",
      page_index: 1,
      page_size: 10
    };
  }

  sortHandler = sort_field => {
    if (sort_field === this.state.sort_field) {
      this.setState(
        {
          sort_order: this.state.sort_order === "desc" ? "asc" : "desc"
        },
        () => {
          console.log("after");
          this.doRefresh();
        }
      );
    } else {
      this.setState(
        {
          sort_field: sort_field,
          sort_order: "desc"
        },
        () => {
          console.log("after");
          this.doRefresh();
        }
      );
    }
  };

  getData = () => {
    return this.props.data;
  };

  pageIndexHandler = value => {
    this.setState({ page_index: Number(value) }, () => this.doRefresh());
  };

  pageSizeHandler = value => {
    this.setState({ page_size: Number(value) }, () => this.doRefresh());
  };

  doRefresh = () => {
    const { onRefresh } = this.props;
    if (onRefresh) {
      onRefresh({ ...this.state });
    }
  };
  componentDidMount() {
    this.doRefresh();
  }

  render() {
    return (
      <div>
        <Pagination
          totalRecords={this.props.totalRecordsCount}
          pageSize={this.state.page_size}
          pageIndexHandler={this.pageIndexHandler}
          pageSizeHandler={this.pageSizeHandler}
        />
        <table className={classes.Table}>
          <thead className={classes.Header}>
            {
              <tr className={classes.Row}>
                {this.props.headers.map(item => {
                  let objProps = {};
                  objProps.key = item.field_name;
                  if (item.sortable) {
                    objProps.onClick = () => this.sortHandler(item.field_name);
                    objProps.className = classes.SortableColumn;
                  }
                  return <th {...objProps}>{item.title}</th>;
                })}
              </tr>
            }
          </thead>
          <tbody>
            {this.getData().map(order => {
              return (
                <tr key={order.order_number} className={classes.Row}>
                  {this.props.headers.map(item => {
                    return (
                      <td key={item.field_name}>{order[item.field_name]}</td>
                    );
                  })}
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}

export default Table;
