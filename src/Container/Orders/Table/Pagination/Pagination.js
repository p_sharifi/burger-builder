import * as React from "react";
import classes from "./Pagination.module.css";

const Pagination = props => {
  const totalPages = () => {
    const { totalRecords, pageSize } = props;
    return Math.ceil(totalRecords / pageSize);
  };

  const totalPageItems = () => {
    const result = [];
    for (let i = 1; i <= totalPages(); i++) {
      result.push(
        <option key={i} value={i}>
          {i}
        </option>
      );
    }
    return result;
  };

  const pageIndexChange = event => {
    props.pageIndexHandler(event.target.value);
  };

  const pageSizeChange = event => {
    props.pageSizeHandler(event.target.value);
  };

  return (
    <div className={classes.Pagination}>
      Page Index:
      <select onChange={pageIndexChange}>{totalPageItems()}</select>
      Page Size:
      <select onChange={pageSizeChange}>
        <option value={10}>10</option>
        <option value={25}>25</option>
        <option value={50}>50</option>
        <option value={100}>100</option>
      </select>
    </div>
  );
};
export default Pagination;
