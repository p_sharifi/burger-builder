import * as React from "react";
import axios from "../../axios";
import Table from "./Table/Table";

class Orders_Old extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      orders: [],
      headers: []
    };
  }

  componentDidMount() {
    axios
      .post("/Order/GetAllOrders", {})
      .then(res => {
        if (res.status === 200)
          this.setState({
            orders: res.data
          });
      })
      .catch(err => {
        console.log(err);
      });
  }

  render() {
    const headers = [
      { title: "Order Number", field_name: "order_number", sortable: true },
      { title: "Creation Date", field_name: "create_date", sortable: false },
      { title: "Total Price", field_name: "total_price", sortable: true }
      // { title: "Comments", field_name: "comments", sortable: false }
    ];
    return (
      <div>
        <Table headers={headers} data={this.state.orders} />
      </div>
    );
  }
}

export default Orders_Old;
