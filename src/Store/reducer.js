import type from "./actionType";

const initState = {
  loading: false,
  isLogin: false
};
export function Reducer(state = initState, action) {
  switch (action.type) {
    case type.SET_LOADING:
      return {
        ...state,
        loading: action.payload
      };
    case type.SET_LOGIN:
      return {
        ...state,
        isLogin: action.payload
      };
  }
  return state;
}
