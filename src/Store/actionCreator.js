import type from "./actionType";

export function setLoading(loading) {
  return {
    type: type.SET_LOADING,
    payload:loading
  }

};

export function setLogin(isLogin) {
  return{
    type:type.SET_LOGIN,
    payload:isLogin
  }

}