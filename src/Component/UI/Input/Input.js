import * as React from "react";
import classes from "./Input.module.css";

const Input = props => {
  return (
    <div>
      <label>
        <b>{props.label}</b>
      </label>
      <input
        className={classes.Input}
        onChange={props.onChange}
        value={props.value}
        type={props.type}
        placeholder={props.placeholder}
        required={props.required}
      />
      {props.validateMessage && (
        <div className={classes.Validate}>{props.validateMessage}</div>
      )}
    </div>
  );
};

export default Input;
