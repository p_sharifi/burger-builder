import * as React from "react";
import classes from "./Button.module.css";
import PropTypes from "prop-types";

class Button extends React.Component {
  static propTypes = {
    label: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired
  };
  static defaultProps = {
    label: "No Label",
    onClick: () => {
      console.log("No action defined.");
    }
  };

  render() {
    const { label, onClick } = this.props;

    return (
      <button className={classes.Button} onClick={onClick}>
        {label}{" "}
      </button>
    );
  }
}

export default Button;
