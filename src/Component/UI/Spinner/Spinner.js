import * as React from "react";
import classes from "./Spinner.module.css";
import { useSelector } from "react-redux";

function Spinner() {
  const loading = useSelector(state => state.loading);
  if (!loading) return null;
  else {
    return (
      <div className={classes.Container}>
        <div className={classes.loader}>Loading...</div>
      </div>
    );
  }
}

export default Spinner;
