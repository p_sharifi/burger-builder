import * as React from "react";
import classes from "./Logo.module.css";
import logo from "./logoImage.png";

function Logo() {
  return (
    <div>
      <img className={classes.Logo} src={logo} alt={"Logo"} />
    </div>
  );
}

export default Logo;
