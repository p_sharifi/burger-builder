import * as React from "react";
import Logo from "./Logo/Logo";
import Menu from "./Menu/Menu";
import Login from "../Header/Login/Login";
import withClassName from "../../../hoc/withClassName";
import classes from "./Header.module.css";

class Header extends React.Component {
  render() {
    return (
      <>
        <Login />
        <Logo />
        <Menu />
      </>
    );
  }
}

export default withClassName(Header, classes.Container);
