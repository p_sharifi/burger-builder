import * as React from "react";
import classes from "./Menu.module.css";
import { NavLink } from "react-router-dom";
import { useSelector } from "react-redux";

function Menu() {
  const isLogin = useSelector(state => state.isLogin);
  console.log(isLogin);
  return (
    <div className={classes.Menu}>
      {isLogin && (
        <NavLink activeClassName={classes.Active} to="/Orders">
          Orders
        </NavLink>
      )}
      <NavLink activeClassName={classes.Active} to="/BurgerBuilder">
        BurgerBuilder
      </NavLink>
    </div>
  );
}

export default Menu;
