import * as React from "react";
import classes from "./Login.module.css";
import { NavLink } from "react-router-dom";
// import { AuthContext } from "../../../../Context/AuthContext";
import useRedux from "../../../../Container/hooks/useRedux";
import { useSelector } from "react-redux";

function Login() {
  // const { isLogin, setLogin } = React.useContext(AuthContext);
  const isLogin = useSelector(state => state.isLogin);
  const { setLogin } = useRedux();

  const logoutHandler = () => {
    localStorage.removeItem("token");
    setLogin(false);
  };

  return isLogin ? (
    <div className={classes.Menu}>
      <NavLink activeClassName={classes.Active} to="Profile">
        Profile
      </NavLink>
      <NavLink
        activeClassName={classes.Active}
        to="Login"
        onClick={logoutHandler}
      >
        LogOut
      </NavLink>
    </div>
  ) : (
    <div className={classes.Menu}>
      <NavLink activeClassName={classes.Active} to="Login">
        Login
      </NavLink>
      <NavLink activeClassName={classes.Active} to="Signup">
        SignUp
      </NavLink>
    </div>
  );
}

export default Login;
