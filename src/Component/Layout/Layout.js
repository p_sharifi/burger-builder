import * as React from "react";
import Header from "./Header/Header";
import "./Layout.css";
// import { AuthContext } from "../../Context/AuthContext";
import axios from "../../axios";
import useRedux from "../../Container/hooks/useRedux";

function Layout(props) {
  // const { setLogin } = React.useContext(AuthContext);
  const { setLogin } = useRedux();
  React.useEffect(() => {
    const token = localStorage.getItem("token");
    axios
      .post("/User/IsTokenValid", {
        token
      })
      .then(res => {
        if (res.data.status) {
          setLogin(true);
        }
      });
  }, []);
  return (
    <div className="Layout">
      <Header />
      <div className="MainBody">{props.children}</div>
    </div>
  );
}

export default Layout;
